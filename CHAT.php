<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
 <head>
  <title>Chat</title>
  <link rel="stylesheet" type="text/css" href="CHAT.css">
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
  <script type="text/javascript" src="css/CHAT.js"></script>
  <script type="text/javascript" src="js/NAV.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
  
  
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
 <!--Coded With Love By Mutiullah Samim-->
 <body>
  <div class="container-fluid h-100">
   <div class="row justify-content-center h-100">
    <div class="col-md-4 col-xl-3 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
     <div class="card-header">
      <div class="input-group">
       <input type="text" placeholder="Search..." name="" class="form-control search">
       <div class="input-group-prepend">
        <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
       </div>
      </div>
     </div>
     <div class="card-body contacts_body">
      <ui class="contacts">
      <li class="active">
       <div class="d-flex bd-highlight">
        <div class="img_cont">
         <img src="img/LELY.jpg" class="rounded-circle user_img">
         <span class="online_icon"></span>
        </div>
        <div class="user_info">
         <span>Leli Deswindi S.E., MT</span>
         <p>Tolong nanti Siang temu saya di ruangan ya</p>
        </div>
       </div>
      </li>
      <li>
       <div class="d-flex bd-highlight">
        <div class="img_cont">
         <img src="img/DESTA.jpg" class="rounded-circle user_img">
         <span class="online_icon offline"></span>
        </div>
        <div class="user_info">
         <span>Desta Sandya S.Kom, M.Kom</span>
         <p>Sebaiknya, kurang kurangin main gamenyaa utha</p>
        </div>
       </div>
      </li>
      <li>
       <div class="d-flex bd-highlight">
                <div class="img_cont">
         <img src="img/ERZA.jpg" class="rounded-circle user_img">
         <span class="online_icon"></span>
        </div>
        <div class="user_info">
         <span>Erza Sofian S.Kom, M.Sc</span>
         <p>Tolong minggu depan laorkan kepada saya ya perkembangannya</p>
        </div>

       </div>
      </li>
      <li>
       <div class="d-flex bd-highlight">
        <div class="img_cont">
         <img src="img/BARIR.jpg" class="rounded-circle user_img">
         <span class="online_icon offline"></span>
        </div>
        <div class="user_info">
         <span>Abdul Barir Hakim S.Kom, MTI</span>
         <p>kurangin mengeluhnya banyakin syukurnya :) </p>
        </div>
       </div>
      </li>
      <li>
       <div class="d-flex bd-highlight">
        <div class="img_cont">
         <img src="img/bayu.jpg" class="rounded-circle user_img">
         <span class="online_icon offline"></span>
        </div>
        <div class="user_info">
         <span>Bayu Kelana S.Kom., M.Kom</span>
         <p><b>Bayu</b> is typing...</p>
        </div>
       </div>
      </li>
      </ui>
     </div>
     <div class="card-footer"></div>
    </div></div>
    <div class="col-md-8 col-xl-6 chat">
     <div class="card">
      <div class="card-header msg_head">
       <div class="d-flex bd-highlight">
        <div class="img_cont">
         <img src="img/BAYU.jpg" class="rounded-circle user_img">
         <span class="online_icon"></span>
        </div>
        <div class="user_info">
         <span>Chat with Bayu Kelana S.Kom., M.Kom</span>
         <p>1767 Messages</p>
        </div>
        <div class="video_cam">
         <span><i class="fas fa-video"></i></span>
         <span><i class="fas fa-phone"></i></span>
        </div>
       </div>
       <span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
       <div class="action_menu">
        <ul>
         <li><i class="fas fa-user-circle"></i> View profile</li>
         <li><i class="fas fa-users"></i><a href="reqTiket.html">request tiket</li></a>
         
        </ul>
       </div>
      </div>
      <div class="card-body msg_card_body">
       
       <div class="d-flex justify-content-end mb-4">
        <div class="msg_cotainer_send">
         Assalamualaikum pak, selamat siang. saya mau konsul mengenai ide bisnis.
         <span class="msg_time_send">8:55 AM, Today</span>
        </div>
        <div class="img_cont_msg">
       <img src="IMG/USER1.png" class="rounded-circle user_img_msg">
        </div>
       </div>
       <div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
         <img src="IMG/BAYU.JPG" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">
         Wa'alaikum salaam. Bisnis tentang apa, Uther-sama
         <span class="msg_time">8:40 AM, Today</span>
        </div>
       </div>
       <div class="d-flex justify-content-end mb-4">
        <div class="msg_cotainer_send">
         Di bidang game dan anime pak.
         <span class="msg_time_send">8:55 AM, Today</span>
        </div>
        <div class="img_cont_msg">
       <img src="IMG/USER1.PNG" class="rounded-circle user_img_msg">
        </div>
       </div>
       <div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
         <img src="IMG/BAYU.JPG" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">
         o.. iyaa iyaa, passion kamu memang di bidang game dan anime ya? 
         <span class="msg_time">9:00 AM, Today</span>
        </div>
       </div>
       <div class="d-flex justify-content-end mb-4">
        <div class="msg_cotainer_send">
         iya pak, passion saya di situ
         <span class="msg_time_send">9:05 AM, Today</span>
        </div>
        <div class="img_cont_msg">
       <img src="IMG/USER1.PNG" class="rounded-circle user_img_msg">
        </div>
       </div>
       <div class="d-flex justify-content-start mb-4">
        <div class="img_cont_msg">
         <img src="IMG/BAYU.JPG" class="rounded-circle user_img_msg">
        </div>
        <div class="msg_cotainer">
         bisa ceritakan utha bagaimana ide bisnisnya?
         <span class="msg_time">9:07 AM, Today</span>
        </div>
       </div>
       <div class="d-flex justify-content-end mb-4">
        <div class="msg_cotainer_send">
         Ya gitu pak ehehehe hazukashii >3<
         <span class="msg_time_send">9:10 AM, Today</span>
        </div>
        <div class="img_cont_msg">
      <img src="IMG/USER1.PNG" class="rounded-circle user_img_msg">
        </div>
       </div>
      </div>
      <div class="card-footer">
       <div class="input-group">
        <div class="input-group-append">
         <span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
        </div>
        <textarea name="" class="form-control type_msg" placeholder="Type your message..."></textarea>
        <div class="input-group-append">

          <?php $result = mysqli_query($mysqli, "SELECT * FROM chat ORDER BY id DESC"); ?>
         <span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
</body>
</html>