<?if($_SESSION['level']=="mahasiswa"){
		header("location:login.php?pesan=gagal");
	}
 
	?>

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->


<!DOCTYPE html>
<html lang="en">

	<head>

		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		  <meta name="description" content="">
		  <meta name="author" content="">

		  <title>KONSELING EBS</title>

		  <!-- Bootstrap core CSS -->
		  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		  <!-- Custom fonts for this template -->
		  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
		  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
		  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

		  <!-- Custom styles for this template -->
		  <link href="css/agency.min.css" rel="stylesheet">
		  <link rel="stylesheet" type="text/css" href="video.css">
		  <link rel="stylesheet" type="text/css" href="NAV.css">
		  <link rel="stylesheet" href="css/linearicons.css">
		  <link rel="stylesheet" href="css/footer.css">
		  <link rel="stylesheet" href="css/NAV.css">
		  
  
			<link rel="stylesheet" href="css/owl.carousel.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/nice-select.css">
			<link rel="stylesheet" href="css/magnific-popup.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/main.css">
			<link rel="stylesheet" href="css/VIDEO.css">
			
	</head>

<body id="page-top">

  <!-- Navigation -->

<!------ Include the above in your HEAD tag ---------->

		<nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-DARK FIXED-TOP">
		  <a><img src="img/Logo1.png" width="60 px"></a>
		  
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			  <li class="nav-item active">
				<a class="nav-link" href="#">
				  <i class="fa fa-home" href="halaman_mahasiswa"></i>
				  Home
				  <span class="sr-only">(current)</span>
				  </a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="CHAT.html">
				   <i class="fa fa-envelope">
					<span class="badge badge-danger">11</span>
				  </i>
				  messege
				</a>
			  </li>
			  
			  <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 
				  <i class="fa fa-book">
					<span class="badge badge-primary">5</span>
				  </i>
				  Article
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				<a class="dropdown-item" href="artikel_pendidikan.php">Pendidikan</a>
				  <a class="dropdown-item" href="artikel_bisnis.php">Bisnis</a>
				  <a class="dropdown-item" href="artikel_starup.php">Startup</a>
				  <a class="dropdown-item" href="artikel_spiritual">Spiritual</a></a>
				  <a class="dropdown-item" href="artikel_pengembangan.php">Pengembangan diri</a>
				  <div class="dropdown-divider"></div>
				  
				</div>
			  </li>
			  
			  
			  
			  
			</ul>
			

			<ul class="navbar-nav ">
			  
			  <li class="nav-item">
				<a class="nav-link" href="#">
				  <i class="fa fa-bell">
					<span class="badge badge-success">11</span>
				  </i>
				  Notifikasi
				</a>
			  </li>
				
				
				<li class="nav-item ">
					<a class="nav-link href="#" id="navbarDropdown">
					<i class="fa fa-info"></i>
						about
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown"><a class="dropdown-item" href="dashboard_admin.php">profile</a>
						<a class="dropdown-item" href="logout.php">logout</a>
					 
					  <div class="dropdown-divider"></div>
					  
					</div>
				</li>
				
				<li class="nav-item dropdown">
				<a class="nav-link" dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 
				 
				   <i class="fa fa-user"></i> 
				  </i>
				  user
				</a>
				 <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="profile_mahasiswa.php">profile</a>
					<a class="dropdown-item" href="logout.php">logout</a>
				 
				  <div class="dropdown-divider"></div>
				  
				</div>
				
				  
				</div>
			  </li>
			  </ul>
			  
		   

		  <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 
				  <button class="btn btn-outline-success my-2 my-sm-0">konseling</button>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
				  <a class="dropdown-item" href="CHAT.HTML">Pendidikan</a>
				  <a class="dropdown-item" href="Chat.html">Bisnis</a>
				  <a class="dropdown-item" href="Chat.html">Startup
				  <a class="dropdown-item" href="Chat.html">Spiritual</a></a>
				  <a class="dropdown-item" href="Chat.html">Pengembangan diri</a>
				  
				  
				</div>
			  </li>
		
			  </div>
		</nav>

<body>


 <!-- Portfolio Grid -->
  <section class="bg-light" id="portfolio">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">
		  </br>
		  </br>
		  
		  <h1>ARTIKEL BISNIS</h1>
          <h3 class="section-subheading text-muted"></h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
            <div class="image">
			<span>
			<img src="https://sleekr.co/wp-content/uploads/2019/03/shutterstock_186761918-min.jpg" width="350 px"></span>
			</div>
          </a>
          <div class="portfolio-caption">
            <h4>Langkah Penting dan Strategi HRD dalam Menghadapi Revolusi Industri 4.0</h4>
            <p>Masa yang baru telah tiba dan sudah berada di depan mata</p>
			<a href="https://sleekr.co/blog/strategi-hrd-jitu-menghadapi-revolusi-industri-4-0/">baca selengkapnya</a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
            <div class="image">
								<span>
								<img src="https://s3-ap-southeast-1.amazonaws.com/swa.co.id/wp-content/uploads/2015/04/2015-04-08_21.12.301.jpg" width="350 px"></span>
			</div>
			</a>
          <div class="portfolio-caption">
            <h4>Seluk Beluk Perjalanan Go-Jek Menjadi Startup Unicorn</h4>
            <p>siapa yang tak kenal nama ini. Berawal dari startup (perusahaan rintisan) kecil,
			</p>
			<a href="https://www.liputan6.com/tekno/read/3282432/seluk-beluk-perjalanan-go-jek-menjadi-startup-unicorn">baca selengkapnya...</a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
           <div class="image">
				<span>
					<img src="https://cdn.moneysmart.id/wp-content/uploads/2018/11/22120516/shutterstock_1149152354-min-700x497.jpg" width="350 px"></span>
				</div>
		</a>
          <div class="portfolio-caption">
            <h4>Tokopedia, Startup E-Commerce Yang Mampu Bertahan Dan Terus Bertumbuh</h4>					</h4>
            <p>Semenjak hype startup tahun 2009/2010 lalu...............</p>
			<a href="https://dailysocial.id/post/tokopedia/">baca selengkapnya...</a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
               <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
			 
            </div>
             <div class="image">
								<span>
								<img src="https://image.cermati.com/q_70/jhxjpa0mdtyls7zj0ajy.webp" width="350 px"></span>
							</div>
          </a>
          <div class="portfolio-caption">
            <h4>17 Startup Sukses Milik Generasi Millenial Indonesia ini Punya 5 Tips yang Bisa Ditiru</h4>
								</h4>
            <p class="text-muted">Tak hanya ide bisnis yang kuat tapi pelajari juga tips-tips cerdas dari startup lain yang sudah sukses.</p>
			<a href="https://www.cermati.com/artikel/17-startup-sukses-milik-generasi-millenial-indonesia-ini-punya-5-tips-yang-bisa-ditiru">baca selengkapnya...</a>
		  </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
			 
            </div>
			 <div class="image">
				<span>
					<img src="https://d26bwjyd9l0e3m.cloudfront.net/wp-content/uploads/2019/02/Group-High-Five-Featured.jpg" width="350 px"></span>
				</div>
           </a>
          <div class="portfolio-caption">
            <h4>Pola dari Semua Kisah Sukses Startup yang Patut Kamu Ketahui</h4>
								</h4>
            <p class="text-muted">Masa depan adalah milik orang-orang yang menceritakan kisah terbaik. Di balik setiap kisah yang menarik terdapat pola universal
</p>
			<a href="https://id.techinasia.com/pola-kisah-sukses-startup">baca selengkapnya....</a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="ionicons ion-ios-book fa-3x"></i>
              </div>
            </div>
            <div class="image">
			<span>
				<img src="https://kissflow.com/wp-content/uploads/2015/10/Accounting-for-Startups-is-often-more-about-the-People.png" width="350 px"></span>
			</div>
          </a>
          <div class="portfolio-caption">
            <h4>Ini 7 Rahasia Sukses Mengembangkan Bisnis Startup</h4>
								</h4>
            <p class="text-muted">startup yang sedang kamu kembangkan bisa berjalan lancar kok, asalkan, kamu bisa menjalani 7 langkah berikut, dengan baik
			</p>
			<a href="https://www.google.com/search?safe=strict&ei=j63ZXNfSC4mesQWv2qvoDA&q=Ini+7+Rahasia+Sukses+Mengembangkan+Bisnis+Startup&oq=Ini+7+Rahasia+Sukses+Mengembangkan+Bisnis+Startup&gs_l=psy-ab.3...2211.2211..2967...0.0..0.0.0.......0....2j1..gws-wiz.WgnimcggBgQ">baca selnengkapnya....</a>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>

 <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="js/jqBootstrapValidation.js"></script>
  <script src="js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/agency.min.js"></script>

</body>

</html>